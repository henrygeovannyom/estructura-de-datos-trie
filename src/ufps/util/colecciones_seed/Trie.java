/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author GeovanyMantilla
 */

public class Trie {

	private TrieNode root;

	public Trie() {
		root = new TrieNode();
	}
     public Trie(String url) {
         
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        String[] fila = null;
        root = new TrieNode();
        for (int i = 0; i < v.length; i++) {
            fila = v[i].toString().split(", ");
        }
        for (int i = 0; i < fila.length; i++) {
           insert(fila[i]);            
        } 
        

    }

	public void insert(String word) {
		TablaHash<Character, TrieNode> children = root.getChildren();
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			TrieNode node;
			if(children.esta(c)) {
				node = (TrieNode) children.getObjeto(c);
			} else { 
				node = new TrieNode(c);
				children.insertar(c, node);
			}
			children = node.getChildren();

			if(i == word.length() - 1) {
				node.setLeaf(true);
			}
		}
	}

	public boolean search(String word) {
		TablaHash<Character, TrieNode> children = root.getChildren();

		TrieNode node = null;
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			if(children.esta(c)) {
				node = (TrieNode) children.getObjeto(c);
				children = node.getChildren();
			} else { 
				node = null;
				break;
			}
		}

		if(node != null && node.isLeaf()) {
			return true;
		} else {
			return false;
		}
	}

}