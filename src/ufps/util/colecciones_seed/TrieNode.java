/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author GeovanyMantilla
 */

public class TrieNode {

    private char c;
    private TablaHash<Character, TrieNode> children = new TablaHash();
    private boolean isLeaf;

    public TrieNode() {
    }

    public TrieNode(char c) {
        this.c = c;
    }

    public TablaHash<Character, TrieNode> getChildren() {
        return children;
    }

    public void setChildren(TablaHash<Character, TrieNode> children) {
        this.children = children;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }
}
