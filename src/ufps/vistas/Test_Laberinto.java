/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.util.Scanner;
import ufps.util.varios.Laberinto;

/**
 *
 * @author GeovanyMantilla
 */
public class Test_Laberinto {
    public static void main(String[] args) {
        //LEO
        Scanner sc = new Scanner(System.in);
        int filas = 9;
        int columnas = 5;
        short laberinto[][] = new short[filas][columnas]; 
        for(int i = 0; i < filas; i++){
           for(int j = 0; j < columnas; j++){
               laberinto[i][j] = sc.nextShort();
           }
       }
       //TERMINO DE LEER
       //INICIO
       Laberinto lab = new Laberinto(laberinto);
       //LANZO EL ALGORTIMO
       lab.getCamino(8, 1, 4, 0, 1, 4);
       // lab.imprimir()
    }
    
}
